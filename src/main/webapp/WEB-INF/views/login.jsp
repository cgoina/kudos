<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<c:url value="/resources" var="resources"></c:url>
<c:url value="/login" var="login" />

<!DOCTYPE html>
<!--[if lt IE 9]><html class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Kudos Login</title>

<!--[if lt IE 9]>
	  <script src="${resources}/js/html5shiv.min.js"></script>
	<![endif]-->

<link href="${resources}/css/uswds.min.css" rel="stylesheet" />
<link href="${resources}/css/kudos.css" rel="stylesheet" />
</head>

<body>

	<div class="main-content" id="main-content">
		<div class="kudos-content usa-content">

			<c:if test="${param.error != null}">
				<div class="usa-alert usa-alert-error" role="alert">
					<div class="usa-alert-body">
						<h3 class="usa-alert-heading">Error Status</h3>
						<p class="usa-alert-text">
							<c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}" />
						</p>
					</div>
				</div>
			</c:if>
			<c:if test="${param.logout != null}">
				<div class="usa-alert usa-alert-success">
					<div class="usa-alert-body">
						<h3 class="usa-alert-heading">Logout Status</h3>
						<p class="usa-alert-text">You have been logged out.</p>
					</div>
				</div>
			</c:if>

			<form class="usa-form" method="post" action="${login}">
				<fieldset>
					<legend class="usa-drop_text">Sign in</legend>
					<span>or <a href="#">create an account</a></span> 
					<label for="email">Email address</label> 
					<input id="email" name="email" type="text" autocapitalize="off" autocorrect="off">
					<label for="password">Password</label> 
					<input id="password" name="password" type="password"> 
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
					<input type="submit" value="Sign in">
					<p>
						<a href="#" title="Forgot password"> Forgot password?</a>
					</p>
				</fieldset>
			</form>

		</div>
	</div>

	<script src="${resources}/js/uswds.min.js"></script>
	<script src="${resources}/js/jquery-3.1.0.min.js"></script>

</body>
</html>