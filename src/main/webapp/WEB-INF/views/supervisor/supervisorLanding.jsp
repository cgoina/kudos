<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<c:url value="/resources" var="resources" />
<c:url value="/logout" var="logout" />
<c:url value="/admin" var="admin" />
<c:url value="/kudos/giveKudos" var="giveKudos" />
<c:url value="/admin/reports" var="reports" />

<!DOCTYPE html>
<!--[if lt IE 9]><html class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Kudos</title>

<!--[if lt IE 9]>
	  <script src="${resources}/js/html5shiv.min.js"></script>
	<![endif]-->

<link href="${resources}/css/uswds.min.css" rel="stylesheet" />
<link href="${resources}/css/kudos.css" rel="stylesheet" />
</head>

<body>
	<a class="skipnav" href="#main-content">Skip to main content</a>
	<header class="usa-site-header" role="banner">
		<div class="site-navbar">
			<a class="menu-btn" href="#">Menu</a>
			<div class="site-logo" id="logo">
				<em>Profile for: ${employee}</em>
			</div>
			<ul class="usa-button-list usa-unstyled-list">
				<li><a class="usa-button usa-button-outline-inverse" onclick="window.location.href = '${logout}'"> Logout </a></li>
			</ul>
		</div>
	</header>

	<div class="main-content" id="main-content">
		<div class="usa-content">
			<div class="usa-grid">
				<div class="usa-width-three-fourths">

					
				</div>
				<div class="usa-width-one-fourth">
					<h3>Kudos stream</h3>
					<table>
						<thead>
						</thead>
						<tbody id="kudosStream"></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<script src="${resources}/js/uswds.min.js"></script>
	<script src="${resources}/js/socksjs-1.1.1.min.js"></script>
	<script src="${resources}/js/stomp.min.js"></script>
	<script src="${resources}/js/jquery-3.1.0.min.js"></script>

	<script>
		//Create stomp client over sockJS protocol
		var socket = new SockJS("/kudos/ws");
		var stompClient = Stomp.over(socket);

		function renderKudos(frame) {
			var kudos = JSON.parse(frame.body);
			$('#kudosStream').empty();
			for ( var i in kudos) {
				var kudo = kudos[i];
				$('#kudosStream').append($('<tr>').append($('<td>').html(kudo.byEmployee + " sent [" + kudo.kudos + "] to " + kudo.forEmployee)));
			}
		}

		// Callback function to be called when stomp client is connected to server
		var connectCallback = function() {
			stompClient.subscribe('/feed/kudos', renderKudos);
		};

		// Callback function to be called when stomp client could not connect to server
		var errorCallback = function(error) {
			alert(error.headers.message);
		};

		// Connect to server via websocket
		stompClient.connect("guest", "guest", connectCallback, errorCallback);
	</script>

</body>
</html>
