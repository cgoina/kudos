<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<c:url value="/resources" var="resources" />
<c:url value="/logout" var="logout" />
<c:url value="/admin" var="admin" />
<c:url value="/employee/processKudos" var="processKudos" />
<c:url value="/kudos/giveKudos" var="giveKudos" />
<c:url value="/admin/reports" var="reports" />

<!DOCTYPE html>
<!--[if lt IE 9]><html class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Kudos</title>

<!--[if lt IE 9]>
	  <script src="${resources}/js/html5shiv.min.js"></script>
	<![endif]-->

<link href="${resources}/css/starrating.css" rel="stylesheet">
<link href="${resources}/css/uswds.min.css" rel="stylesheet" />
<link href="${resources}/css/kudos.css" rel="stylesheet" />
</head>

<body>
	<a class="skipnav" href="#main-content">Skip to main content</a>
	<header class="usa-site-header" role="banner">
		<div class="site-navbar">
			<a class="menu-btn" href="#">Menu</a>
			<div class="site-logo" id="logo">
				<em>Employee Profile for: ${employee}</em>
			</div>
			<ul class="usa-button-list usa-unstyled-list">
				<li><a class="usa-button usa-button-outline-inverse" onclick="window.location.href = '${logout}'"> Logout </a></li>
			</ul>
		</div>
	</header>

	<div class="main-content" id="main-content">
		<div class="usa-content">
			<div class="usa-grid">
				<div class="usa-width-three-fourths">

					<!-- if ranking is in 0-33% then color it green, 33%-66% yellow, 66%-100% red -->
					<c:choose>
						<c:when test="${(ranking > 0) and (ranking <= percentile33) }">
							<div id="percentageId" class="usa-alert usa-alert-success usa-alert-no_icon" role="alert">
						</c:when>
						<c:when test="${(ranking > percentile33) and (ranking <= percentile66) }">
							<div id="percentageId" class="usa-alert usa-alert-warning usa-alert-no_icon" role="alert">
						</c:when>
						<c:otherwise>
							<div id="percentageId" class="usa-alert usa-alert-error usa-alert-no_icon" role="alert">
						</c:otherwise>
					</c:choose>
					<div class="usa-alert-body">
						<p class="usa-alert-text">
							<c:out value="My Kudos Ranking: ${ranking}" />
						</p>
					</div>
				</div>


				<p>Current count of my kudos: ${myKudos.size()}</p>
				<Button class="accordion">Send Kudos</Button>
				<div class="panel">
					<form:form class="usa-form" method="post" action="${processKudos}" modelAttribute="kudosForm">
						<fieldset>
							<legend>Kudos</legend>
							<label for="forEmployee">For User <span class="usa-additional_text">Required</span></label> <select name="forEmployee" id="forEmployee">
								<c:forEach var="u" items="${allusers}">
									<option value="${u.id}">${u.firstName} ${u.lastName}</option>
								</c:forEach>
							</select> <label for="projectId">Project</label> <select name="projectId" id="projectId">
								<c:forEach var="proj" items="${allProjects}">
									<option value="${proj.id}">${proj.projectName}</option>
								</c:forEach>
							</select>
							<div>
								<br><br>
								Rating:
								<div class="rating">
									<input type="radio" id="rating5" name="rating" value="FIVE"><label class="full" for="rating5"></label>
									<input type="radio" id="rating4" name="rating" value="FOUR"><label class="full" for="rating4"></label>
									<input type="radio" id="rating3" name="rating" value="THREE"><label class="full" for="rating3"></label>
									<input type="radio" id="rating2" name="rating" value="TWO"><label class="full" for="rating2"></label>
									<input type="radio" id="rating1" name="rating" value="ONE"><label class="full" for="rating1"></label>
								</div>
								<input id="rating0" type="radio" name="rating" value="ZERO" hidden="" checked="">
								<label for="rating0" hidden="">0</label>
							</div>
							<label for="kudos">Kudos <span class="usa-additional_text">Required</span></label>
							<textarea id="kudos" name="kudos" type="text" required="" aria-required="true" maxlength="250"></textarea>
							<button type="submit" name="go">Submit</button>
							<button type="reset">Reset</button>
						</fieldset>
					</form:form>
				</div>
				<button class="accordion">My Kudos</button>
				<div class="panel">
					<h2>My Kudos</h2>
					<c:if test="${not empty myKudos}">
						<div>
							<table>
								<thead>
									<tr>
										<th>Kudos</th>
										<th>By User</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="k" items="${myKudos}">
										<tr>
											<td>${k.kudos}</td>
											<td><c:out value="${k.byEmployee.firstName} ${k.byEmployee.lastName}" /></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</c:if>
				</div>
				<button class="accordion">My Info</button>
				<div class="panel">
					<h2>My Info</h2>
					<table>
						<tr>
							<td><c:out value="${employeeObj.firstName} ${employeeObj.lastName}"/><c:if test="${employeeObj.position != null}"><c:out value=", ${employeeObj.position}" /></c:if></td>
							<td><c:out value="${employeeObj.email}" /></td>
						</tr>
						<tr>
							<td>Manager1: <c:if test="${employeeObj.manager1 != null}"><c:out value=" ${employeeObj.manager1.firstName} ${employeeObj.manager1.lastName}" /></c:if></td>
							<td>Manager2: <c:if test="${employeeObj.manager2 != null}"><c:out value=" ${employeeObj.manager2.firstName} ${employeeObj.manager2.lastName}" /></c:if></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="usa-width-one-fourth">
				<h3>Kudos stream</h3>
				<table>
					<thead>
					</thead>
					<tbody id="kudosStream"></tbody>
				</table>
			</div>
		</div>
	</div>
	</div>

	<script src="${resources}/js/uswds.min.js"></script>
	<script src="${resources}/js/socksjs-1.1.1.min.js"></script>
	<script src="${resources}/js/stomp.min.js"></script>
	<script src="${resources}/js/jquery-3.1.0.min.js"></script>

	<script>
		//Create stomp client over sockJS protocol
		var socket = new SockJS("/kudos/ws");
		var stompClient = Stomp.over(socket);

		function renderKudos(frame) {
			var kudos = JSON.parse(frame.body);
			$('#kudosStream').empty();
			for ( var i in kudos) {
				var kudo = kudos[i];
				$('#kudosStream').append($('<tr>').append($('<td>').html(kudo.byEmployee + " sent [" + kudo.kudos + "] to " + kudo.forEmployee)));
			}
		}

		// Callback function to be called when stomp client is connected to server
		var connectCallback = function() {
			stompClient.subscribe('/feed/kudos', renderKudos);
		};

		// Callback function to be called when stomp client could not connect to server
		var errorCallback = function(error) {
			alert(error.headers.message);
		};

		// Connect to server via websocket
		stompClient.connect("guest", "guest", connectCallback, errorCallback);
	</script>

	<script>
		$(document).ready(function() {

			$("#kudosButton").click(function() {
				window.location.href = "${giveKudos}"
			});

			$('input[type=radio]').click(function() {
				if (this.previous) {
					this.checked = false;
					$('#rating0').checked = true;
				}
				this.previous = this.checked;
			});
		});
	</script>
	<script>
		var acc = document.getElementsByClassName("accordion");
		var i;

		for (i = 0; i < acc.length; i++) {
			acc[i].onclick = function(){
				this.classList.toggle("active");
				this.nextElementSibling.classList.toggle("show");
			}
		}
	</script>
	<!--
		<script>
			$("#uploadForm").submit(function (event) {
				event.preventDefault();
				//grab all form data
				var formData = new FormData($(this)[0]);

				$.ajax({
					url: 'http://52.0.199.20:8080/kudos/kudos/processKudos',
					type: 'POST',
					data: formData,
					dataType: 'json',
					cache: false,
					contentType: 'application/json',
					processData: false
				}).done(function () {
					console.log("done");
				});
			});
		</script>
	-->
</body>
</html>
