/*
 *
 */
package gov.dhs.kudos.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "STAR_RATING")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class StarRating implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "TYPE", unique = true, nullable = false)
	private StarRatingType type = StarRatingType.ZERO;

	public long getId() {
		return id;
	}

	public void setId(final long id) {
		this.id = id;
	}

	public StarRatingType getType() {
		return type;
	}

	public void setType(final StarRatingType type) {
		this.type = type;
	}

	public StarRating() {

	}

	public StarRating(final StarRatingType role) {
		setType(role);
		setId(role.ordinal() + 1);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof StarRating)) {
			return false;
		}
		final StarRating other = (StarRating) obj;
		if (type == null) {
			if (other.type != null) {
				return false;
			}
		} else if (!type.equals(other.type)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}