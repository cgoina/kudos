/*
 *
 */
package gov.dhs.kudos.repository;

import java.util.List;

import gov.dhs.kudos.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import gov.dhs.kudos.model.Role;
import gov.dhs.kudos.model.State;

@Transactional
public interface UserRepository extends JpaRepository<Employee, Long> {
	public Employee findByEmail(String email);

	public List<Employee> findByState(State state);

	public List<Employee> findByRole(Role role);

	@Query("SELECT t1 FROM " +
			"Employee t1 " +
			"WHERE t1.manager1=(?1) " +
			"OR t1.manager2=(?1)")
	public List<Employee> findByManager1OrManager2(Employee manager);

}