package gov.dhs.kudos.test;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import gov.dhs.kudos.SecurityConfiguration;
import gov.dhs.kudos.model.Employee;
import gov.dhs.kudos.model.Role;
import gov.dhs.kudos.model.RoleType;
import gov.dhs.kudos.model.State;
import gov.dhs.kudos.repository.RoleRepository;
import gov.dhs.kudos.repository.UserRepository;
import gov.dhs.kudos.test.config.TestRootConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestRootConfig.class, SecurityConfiguration.class })
@WebAppConfiguration
public class LoginTest {

	private static final String LOGIN_ERROR_URL = "/login?error";

	private static final String ACCESSDENIED_URL = "/accessdenied";

	@Autowired
	protected WebApplicationContext context;

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private RoleRepository roleRepo;

	protected MockMvc mockMvc;

	private Role adminRole;
	private Role userRole;
	private Role managerRole;

	final private Employee employee1 = new Employee("Bill", "Hunt", "password1", "bill.hunt@clearavenue.com", State.APPROVED);
	final private Employee manager1 = new Employee("Chan", "Smith", "password1", "chandler.smith@clearavenue.com", State.APPROVED);
	final private Employee admin1 = new Employee("Shane", "Hogan", "password1", "shane.hogan@clearavenue.com", State.APPROVED);

	@Before
	public void setup() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).alwaysDo(print()).apply(springSecurity()).build();

		adminRole = roleRepo.save(new Role(RoleType.ADMIN));
		userRole = roleRepo.save(new Role(RoleType.EMPLOYEE));
		managerRole = roleRepo.save(new Role(RoleType.SUPERVISOR));

		admin1.setRole(adminRole);
		employee1.setRole(userRole);
		manager1.setRole(managerRole);

		userRepo.save(Arrays.asList(admin1, employee1, manager1));
	}

	@After
	public void teardown() throws Exception {
		userRepo.deleteAll();
		roleRepo.deleteAll();
	}

	@Test
	public void testPostLoginForbidden() throws Exception {
		mockMvc.perform(post("/login")).andExpect(status().isForbidden()).andExpect(forwardedUrl(ACCESSDENIED_URL));
	}

	@Test
	public void testGetLogin() throws Exception {
		mockMvc.perform(get("/login")).andExpect(status().isOk());
	}

	@Test
	public void testLogoutGet() throws Exception {
		mockMvc.perform(get("/logout")).andExpect(status().isOk());
	}

	@Test
	public void testLogout() throws Exception {
		mockMvc.perform(logout()).andExpect(status().isFound()).andExpect(unauthenticated());
	}

	@Test
	@WithAnonymousUser
	public void authenticationFailedAnonymous() throws Exception {
		mockMvc.perform(formLogin()).andExpect(status().isFound()).andExpect(redirectedUrl(LOGIN_ERROR_URL)).andExpect(unauthenticated());
	}

	@Test
	@WithMockUser
	public void authenticationFailedUser() throws Exception {
		mockMvc.perform(formLogin()).andExpect(status().isFound()).andExpect(redirectedUrl(LOGIN_ERROR_URL)).andExpect(unauthenticated());
	}

	@Test
	public void authenticationFailedBadPwd() throws Exception {
		mockMvc.perform(formLogin().user("email", "bill.hunt@clearavenue.com").password("password", "invalid")).andExpect(status().isFound())
				.andExpect(redirectedUrl(LOGIN_ERROR_URL)).andExpect(unauthenticated());
	}

	@Test
	public void authenticationFailedBadUser() throws Exception {
		mockMvc.perform(formLogin().user("email", "invalid@email.com").password("password", "invalid")).andExpect(status().isFound()).andExpect(redirectedUrl(LOGIN_ERROR_URL))
				.andExpect(unauthenticated());
	}

	@Test
	public void authenticationSuccess() throws Exception {
		mockMvc.perform(formLogin().user("email", "bill.hunt@clearavenue.com").password("password", "password1")).andExpect(status().isFound()).andExpect(redirectedUrl("/"))
				.andExpect(authenticated().withUsername("bill.hunt@clearavenue.com"));
	}

}