package gov.dhs.kudos.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Ranking {
	private int ranking;
	private double percentile33;
	private double percentile66;

	public Ranking() {
	}

	public Ranking(final int r, final double p33, final double p66) {
		ranking = r;
		percentile33 = p33;
		percentile66 = p66;
	}

	public int getRanking() {
		return ranking;
	}

	public void setRanking(final int r) {
		ranking = r;
	}

	public double getPercentile33() {
		return percentile33;
	}

	public void setPercentile33(final double percentile33) {
		this.percentile33 = percentile33;
	}

	public double getPercentile66() {
		return percentile66;
	}

	public void setPercentile66(final double percentile66) {
		this.percentile66 = percentile66;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}