<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<c:url value="/resources" var="resources" />
<c:url value="/admin" var="admin" />
<c:url value="/logout" var="logout" />

<!DOCTYPE html>
<!--[if lt IE 9]><html class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Select User</title>

<!--[if lt IE 9]>
	  <script src="${resources}/js/html5shiv.min.js"></script>
	<![endif]-->

<link href="${resources}/css/uswds.min.css" rel="stylesheet" />
<link href="${resources}/css/kudos.css" rel="stylesheet" />
</head>

<body>
	<a class="skipnav" href="#main-content">Skip to main content</a>
	<header class="usa-site-header" role="banner">
		<div class="site-navbar">
			<a class="menu-btn" href="#">Menu</a>
			<div class="site-logo" id="logo">
				<em>Select User</em>
			</div>
			<ul class="usa-button-list usa-unstyled-list">
				<li><a class="usa-button usa-button-outline-inverse" onclick="window.location.href = '${admin}'"> Back </a></li>
				<li><a class="usa-button usa-button-outline-inverse" onclick="window.location.href = '${logout}'"> Logout </a></li>
			</ul>
		</div>
	</header>
	<div class="main-content usa-center" id="main-content">
		<div class="kudos-content usa-content">
			<table>
				<thead>
					<tr>
						<th scope="col">ID</th>
						<th scope="col">FirstName</th>
						<th scope="col">LastName</th>
						<th scope="col">Email</th>
						<th scope="col">Role</th>
						<th scope="col">Manager1</th>
						<th scope="col">Manager2</th>
						<th scope="col">Action</th>
					</tr>
				</thead>

				<c:forEach var="u" items="${allusers}">
					<tr>
						<td>${u.id}</td>
						<td>${u.firstName}</td>
						<td>${u.lastName}</td>
						<td>${u.email}</td>
						<td>${u.role.type}</td>
						<td><c:out value="${u.manager1.firstName} ${u.manager1.lastName}" /></td>
						<td><c:out value="${u.manager2.firstName} ${u.manager2.lastName}" /></td>
						<td><spring:url value="/admin/modifyUser/${u.id}" var="userUrl" />
							<button onclick="location.href='${userUrl}'">Update</button>
					</tr>
				</c:forEach>
			</table>

		</div>
	</div>

	<script src="${resources}/js/uswds.min.js"></script>
	<script src="${resources}/js/jquery-3.1.0.min.js"></script>

</body>
</html>
