/*.contain
 *
 */
package gov.dhs.kudos.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import gov.dhs.kudos.model.Employee;
import gov.dhs.kudos.model.Kudos;
import gov.dhs.kudos.model.Project;
import gov.dhs.kudos.model.Role;
import gov.dhs.kudos.model.RoleType;
import gov.dhs.kudos.model.StarRating;
import gov.dhs.kudos.model.StarRatingType;
import gov.dhs.kudos.model.State;
import gov.dhs.kudos.repository.KudosRepository;
import gov.dhs.kudos.repository.ProjectRepository;
import gov.dhs.kudos.repository.RoleRepository;
import gov.dhs.kudos.repository.StarRatingRepository;
import gov.dhs.kudos.repository.UserRepository;
import gov.dhs.kudos.test.config.TestJpaConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestJpaConfig.class })
public class KudosRepoTest {

	private static Logger LOGGER = LoggerFactory.getLogger(KudosRepoTest.class);

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private RoleRepository roleRepo;

	@Autowired
	private KudosRepository kudosRepo;

	@Autowired
	private StarRatingRepository starRepo;

	@Autowired
	private ProjectRepository projectRepo;

	Role adminRole;
	Role userRole;
	Role managerRole;

	StarRating zeroStar;
	StarRating oneStar;
	StarRating twoStar;
	StarRating threeStar;
	StarRating fourStar;
	StarRating fiveStar;

	Project dhs;
	Project va;
	Project salesforce;

	Employee employee1 = new Employee("Bill", "Hunt", "password1", "bill.hunt@clearavenue.com", State.APPROVED);
	Employee employee2 = new Employee("John", "Doe", "password1", "john.doe@clearavenue.com", State.APPROVED);
	Employee manager1 = new Employee("Chan", "Smith", "password1", "chandler.smith@clearavenue.com", State.APPROVED);
	Employee manager2 = new Employee("Shane", "Hogan", "password1", "shane.hogan@clearavenue.com", State.APPROVED);

	@Before
	public void loadDB() {
		// add roles
		adminRole = roleRepo.save(new Role(RoleType.ADMIN));
		userRole = roleRepo.save(new Role(RoleType.EMPLOYEE));
		managerRole = roleRepo.save(new Role(RoleType.SUPERVISOR));

		// add star ratings
		zeroStar = starRepo.save(new StarRating(StarRatingType.ZERO));
		oneStar = starRepo.save(new StarRating(StarRatingType.ONE));
		twoStar = starRepo.save(new StarRating(StarRatingType.TWO));
		threeStar = starRepo.save(new StarRating(StarRatingType.THREE));
		fourStar = starRepo.save(new StarRating(StarRatingType.FOUR));
		fiveStar = starRepo.save(new StarRating(StarRatingType.FIVE));

		// add projects
		dhs = projectRepo.save(new Project("DHS"));
		va = projectRepo.save(new Project("VA"));
		salesforce = projectRepo.save(new Project("Salesforce"));

		// add employees
		employee1.setRole(adminRole);
		employee1 = userRepo.save(employee1);
		employee2.setRole(userRole);
		employee2 = userRepo.save(employee2);
		manager1.setRole(managerRole);
		manager1 = userRepo.save(manager1);
		manager2.setRole(managerRole);
		manager2 = userRepo.save(manager2);
	}

	@After
	public void cleanDB() {
		kudosRepo.deleteAll();
		projectRepo.deleteAll();
		starRepo.deleteAll();
		userRepo.deleteAll();
		roleRepo.deleteAll();
	}

	@Test
	public void addKudos() {
		final Date date = new Date();

		final Kudos kudos = new Kudos();
		kudos.setByEmployee(manager1);
		kudos.setForEmployee(employee1);
		kudos.setProject(dhs);
		kudos.setKudos("test kudos");
		kudos.setStarRating(oneStar);

		assertEquals(0, kudosRepo.count());
		kudosRepo.save(kudos);
		assertEquals(1, kudosRepo.count());

		final Kudos saved = kudosRepo.findOne(kudos.getId());
		assertEquals(manager1, saved.getByEmployee());
		assertEquals(employee1, saved.getForEmployee());
		assertEquals(date, saved.getSent());
		assertEquals(dhs, saved.getProject());
		assertEquals(oneStar, saved.getStarRating());
	}

	@Test(expected = DataIntegrityViolationException.class)
	public void addKudosNoByEmployee() {
		final Kudos kudos = new Kudos();
		kudos.setForEmployee(employee1);
		kudos.setProject(dhs);
		kudos.setKudos("test kudos");
		kudos.setStarRating(oneStar);

		kudosRepo.save(kudos);
	}

	@Test(expected = DataIntegrityViolationException.class)
	public void addKudosNoProject() {
		final Kudos kudos = new Kudos();
		kudos.setByEmployee(manager1);
		kudos.setForEmployee(employee1);
		kudos.setKudos("test kudos");
		kudos.setStarRating(oneStar);

		kudosRepo.save(kudos);
	}

	@Test(expected = DataIntegrityViolationException.class)
	public void addKudosNoRating() {
		final Kudos kudos = new Kudos();
		kudos.setByEmployee(manager1);
		kudos.setForEmployee(employee1);
		kudos.setProject(dhs);
		kudos.setKudos("test kudos");

		kudosRepo.save(kudos);
	}

	@Test(expected = DataIntegrityViolationException.class)
	public void addKudosNoKudos() {
		final Kudos kudos = new Kudos();
		kudos.setByEmployee(manager1);
		kudos.setForEmployee(employee1);
		kudos.setProject(dhs);
		kudos.setStarRating(oneStar);

		kudosRepo.save(kudos);
	}

	@Test(expected = DataIntegrityViolationException.class)
	public void addKudosNoForEmployee() {
		final Kudos kudos = new Kudos();
		kudos.setByEmployee(manager1);
		kudos.setProject(dhs);
		kudos.setKudos("test kudos");
		kudos.setStarRating(oneStar);

		kudosRepo.save(kudos);
	}

	@Test
	public void getKudosForEmployee() {
		final Kudos kudos = new Kudos();
		kudos.setByEmployee(manager1);
		kudos.setForEmployee(employee1);
		kudos.setProject(dhs);
		kudos.setKudos("test kudos");
		kudos.setStarRating(oneStar);
		kudosRepo.save(kudos);

		employee1 = userRepo.findOne(employee1.getId());

		final List<Kudos> k = employee1.getKudos();
		assertEquals(1, k.size());
	}

	@Test
	public void getMultipleKudosForEmployee() {
		final Kudos kudos1 = new Kudos();
		kudos1.setByEmployee(manager1);
		kudos1.setForEmployee(employee1);
		kudos1.setProject(dhs);
		kudos1.setKudos("test kudos1");
		kudos1.setStarRating(oneStar);

		final Kudos kudos2 = new Kudos();
		kudos2.setByEmployee(manager2);
		kudos2.setForEmployee(employee1);
		kudos2.setProject(va);
		kudos2.setKudos("test kudos2");
		kudos2.setStarRating(twoStar);

		final List<Kudos> expectedList = Arrays.asList(kudos1, kudos2);
		kudosRepo.save(expectedList);

		employee1 = userRepo.findOne(employee1.getId());

		final List<Kudos> actualList = employee1.getKudos();
		assertEquals(2, actualList.size());
		assertTrue(CollectionUtils.containsAll(actualList, expectedList));
		assertTrue(CollectionUtils.containsAll(expectedList, actualList));
	}

}
