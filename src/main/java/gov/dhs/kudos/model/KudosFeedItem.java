package gov.dhs.kudos.model;

public class KudosFeedItem {

	private String kudos;
	private String byEmployee;
	private String forEmployee;

	public KudosFeedItem() {
	}

	public KudosFeedItem(final String byUserVal, final String forUserVal, final String kudosVal) {
		kudos = kudosVal;
		byEmployee = byUserVal;
		forEmployee = forUserVal;
	}

	public String getKudos() {
		return kudos;
	}

	public void setKudos(final String kudos) {
		this.kudos = kudos;
	}

	public String getByEmployee() {
		return byEmployee;
	}

	public void setByEmployee(final String byEmployee) {
		this.byEmployee = byEmployee;
	}

	public String getForEmployee() {
		return forEmployee;
	}

	public void setForEmployee(final String forEmployee) {
		this.forEmployee = forEmployee;
	}

}
