package gov.dhs.kudos.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import gov.dhs.kudos.model.Employee;
import gov.dhs.kudos.model.Project;
import gov.dhs.kudos.model.Role;
import gov.dhs.kudos.model.RoleType;
import gov.dhs.kudos.model.StarRating;
import gov.dhs.kudos.model.StarRatingType;
import gov.dhs.kudos.model.State;

@Controller
public class SecurityController extends BaseController {

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(final Model model) {
		return "login";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(final HttpServletRequest request, final HttpServletResponse response) {
		final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login?logout";
	}

	// TODO: Remove before production
	@RequestMapping(value = "/initData", method = RequestMethod.GET)
	public String initData() {

		kudosRepo.deleteAll();
		projectRepo.deleteAll();
		starRepo.deleteAll();
		userRepo.deleteAll();
		roleRepo.deleteAll();

		// add roles
		final Role adminRole = roleRepo.save(new Role(RoleType.ADMIN));
		final Role userRole = roleRepo.save(new Role(RoleType.EMPLOYEE));
		final Role managerRole = roleRepo.save(new Role(RoleType.SUPERVISOR));

		// add employees
		final Employee admin = new Employee("Bill", "Hunt", "password1", "admin@clearavenue.com", State.APPROVED);
		admin.setRole(adminRole);
		final Employee manager = new Employee("Srini", "Kankanahalli", "password1", "manager@clearavenue.com", State.APPROVED);
		manager.setRole(managerRole);
		final Employee employee1 = new Employee("Shane", "Hogan", "password1", "user.one@clearavenue.com", State.APPROVED);
		employee1.setRole(userRole);
		final Employee employee2 = new Employee("Chandler", "Smith", "password1", "user.two@clearavenue.com", State.APPROVED);
		employee2.setRole(userRole);
		userRepo.save(Arrays.asList(employee1, employee2, admin, manager));

		// add star ratings
		starRepo.save(new StarRating(StarRatingType.ZERO));
		starRepo.save(new StarRating(StarRatingType.ONE));
		starRepo.save(new StarRating(StarRatingType.TWO));
		starRepo.save(new StarRating(StarRatingType.THREE));
		starRepo.save(new StarRating(StarRatingType.FOUR));
		starRepo.save(new StarRating(StarRatingType.FIVE));

		// add projects
		projectRepo.save(new Project("Bluejay"));
		projectRepo.save(new Project("Cardinal"));
		projectRepo.save(new Project("Eagle"));
		projectRepo.save(new Project("Robin"));

		return "redirect:/";
	}
}
