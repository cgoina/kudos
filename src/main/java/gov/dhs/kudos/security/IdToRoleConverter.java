package gov.dhs.kudos.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import gov.dhs.kudos.model.Role;
import gov.dhs.kudos.repository.RoleRepository;

@Component
public class IdToRoleConverter implements Converter<Object, Role> {

	@Autowired
	RoleRepository roleRepo;

	/*
	 * Gets UserProfile by Id
	 *
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang. Object)
	 */
	@Override
	public Role convert(final Object element) {
		final Long id = Long.parseLong((String) element);
		final Role profile = roleRepo.findOne(id);
		return profile;
	}

}