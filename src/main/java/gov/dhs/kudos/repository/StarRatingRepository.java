package gov.dhs.kudos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import gov.dhs.kudos.model.StarRating;
import gov.dhs.kudos.model.StarRatingType;

@Transactional
public interface StarRatingRepository extends JpaRepository<StarRating, Long> {

	StarRating findByType(StarRatingType type);

}
