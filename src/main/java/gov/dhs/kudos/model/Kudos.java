/*
 *
 */
package gov.dhs.kudos.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@Entity
@Table(name = "KUDOS")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Kudos implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "KUDOS_ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false)
	private String kudos;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date sent;

	@ManyToOne(optional = false)
	private Project project;

	@ManyToOne(optional = false)
	private StarRating starRating;

	@ManyToOne(optional = false)
	private Employee byEmployee;

	@ManyToOne(optional = false)
	private Employee forEmployee;

	public long getId() {
		return id;
	}

	public void setId(final long val) {
		id = val;
	}

	public String getKudos() {
		return kudos;
	}

	public void setKudos(final String val) {
		kudos = val;
	}

	public Employee getByEmployee() {
		return byEmployee;
	}

	public void setByEmployee(final Employee val) {
		byEmployee = val;
	}

	public Employee getForEmployee() {
		return forEmployee;
	}

	public void setForEmployee(final Employee val) {
		forEmployee = val;
	}

	@SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "This is what we want the field called")
	public Date getSent() {
		return sent;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(final Project project) {
		this.project = project;
	}

	public StarRating getStarRating() {
		return starRating;
	}

	public void setStarRating(final StarRating starRating) {
		this.starRating = starRating;
	}

	public Kudos() {
		this.sent = new Date();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((byEmployee == null) ? 0 : byEmployee.hashCode());
		result = prime * result + ((forEmployee == null) ? 0 : forEmployee.hashCode());
		result = prime * result + ((kudos == null) ? 0 : kudos.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Kudos other = (Kudos) obj;
		if (byEmployee == null) {
			if (other.byEmployee != null) {
				return false;
			}
		} else if (!byEmployee.equals(other.byEmployee)) {
			return false;
		}
		if (forEmployee == null) {
			if (other.forEmployee != null) {
				return false;
			}
		} else if (!forEmployee.equals(other.forEmployee)) {
			return false;
		}
		if (kudos == null) {
			if (other.kudos != null) {
				return false;
			}
		} else if (!kudos.equals(other.kudos)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}